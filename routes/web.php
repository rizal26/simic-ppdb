<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(!(Session::get('login'))){
        return redirect('ppdb/login');
    }
    return redirect('ppdb/home');
});


Route::group(['prefix' => 'ppdb'], function () {
  Route::get('login', 'Ppdb\LoginController@index')->name("ppdb_login.index");
  Route::post('login', 'Ppdb\LoginController@store')->name("ppdb_login.store");
  Route::get('home', 'Ppdb\HomeController@index')->name("ppdb_home.index");
  Route::get('logout', 'Ppdb\LoginController@logout')->name("ppdb_login.logout");
  Route::get('forgot_password', 'Ppdb\LoginController@forgot_password')->name("ppdb_login.forgot_password");
  Route::post('forgot_password', 'Ppdb\LoginController@forgot_password_post')->name("ppdb_login.forgot_password");
  Route::get('register', 'Ppdb\RegistrationController@index')->name("ppdb_register.index");
  Route::post('register', 'Ppdb\RegistrationController@store')->name("ppdb_register.store");
  Route::get('profile', 'Ppdb\ProfileController@show')->name("ppdb_profile.show");
  Route::get('profile/datatable', 'Ppdb\ProfileController@datatable')->name("ppdb_profile.datatable");
  Route::get('profile/detail/{id}', 'Ppdb\ProfileController@detail')->name("ppdb_profile.detail");
  Route::get('profile/edit/{id}', 'Ppdb\ProfileController@edit')->name("ppdb_profile.edit");
  Route::post('profile/update/{id}', 'Ppdb\ProfileController@update')->name("ppdb_profile.update");
  Route::get('profile/print', 'Ppdb\ProfileController@print')->name("ppdb_profile.print");

  # show all registrations data
  Route::get('admin', 'Ppdb\AdminController@index')->name("ppdb_admin.index");
  Route::get('admin/edit_student/{id}', 'Ppdb\AdminController@edit_student')->name("ppdb_admin.edit_student");
  Route::post('admin/update_student', 'Ppdb\AdminController@update_student')->name("ppdb_admin.update_student");
});
