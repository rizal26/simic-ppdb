            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>
                            

                        	<li class="text-muted menu-title">Menu </li>

                            <li>
                                <a href="{{ url('/ppdb/home') }}" class="waves-effect"><i class="ti-home"></i> <span> Home </span></a>
                            </li>
                            <li>
                                <a href="{{ url('/ppdb/profile') }}" class="waves-effect"><i class="ti-user"></i> <span> Profile </span> </a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <!-- Left Sidebar End -->