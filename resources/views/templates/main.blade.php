<!DOCTYPE html>
<html>
    <head>
    @include('templates.header')
    </head>
    <body class="fixed-left">
        <!-- Begin page -->
        <div id="wrapper">
            @include('templates.top')
            @include('templates.sidebar')
            <div class="content-page">
                <!-- Start content -->
                <div class="content">
                    <div class="container">
                    @yield('content')
                    </div> <!-- container -->
                </div> <!-- content -->
                @include('templates.footer')
            </div>
        </div>
        <!-- END wrapper -->
        @include('templates.js')
    </body>
</html>
