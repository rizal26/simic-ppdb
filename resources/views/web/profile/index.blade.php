@extends('templates.main')
@section('content')

              <!-- Page-Title -->
              <div class="row">
                    <div class="col-sm-12">

                        {{-- <div class="btn-group pull-right m-t-15">
                            <a href="" class="btn btn-default dropdown-toggle waves-effect">Add <span class="m-l-5"><i class="fa fa-plus"></i></span></a>
                        </div> --}}

                        <h4 class="page-title">Profile Management</h4>
                        <ol class="breadcrumb">
                            {{-- <li>
                                <a href="#">List</a>
                            </li> --}}
                        </ol>
                    </div>
                </div>

                {{-- profile --}}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box table-responsive">
                            {{-- <h4 class="page-title"></h4> --}}
                            <p class="text-muted font-13 m-b-20"></p>
                            <br>
                            @if($errors->any())
                                @foreach($errors->all() as $error)
                                    @if($errors->has('success'))
                                        <div class="alert alert-success">
                                            <strong>{{ $error }}</strong>
                                        </div>
                                    @else
                                        <div class="alert alert-danger">
                                            <strong>{{ $error }}</strong>
                                        </div>
                                    @endif    
                                @endforeach
                            @endif
                            <table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" colspan="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>NISN</th>
                                        <th>Nama Lengkap</th>
                                        <th>Email</th>
                                        <th>Alamat</th>
                                        <th>Tanggal Buat</th>
                                        <th>Tanggal Update</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>

                <div id="detail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-lg"> 
                        <div class="modal-content "> 
                            <div class="modal-header"> 
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                <h4 class="modal-title">Detil Informasi</h4> 
                            </div> 
                            <div class="modal-body">
                                <div id="loading"></div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <table class="table table-striped">
                                            <tbody id="bodyLeft"></tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-6">
                                        <table class="table table-striped">
                                            <tbody id="bodyRight"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div> 
                            <div class="modal-footer"> 
                                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Keluar</button> 
                            </div> 
                        </div> 
                    </div>
                </div><!-- /.modal -->      

                <script type="text/javascript">
                    $(function() {
                        'use strict';

                        $('#detail').on('show.bs.modal', function (event) {
                            var button = $(event.relatedTarget) 
                            var id = button.data('id')
                            var modal = $(this)
                            modal.find('.modal-body #id').val(id)

                            $.ajax({
                                type: "get",
                                url: "{{ url('ppdb/profile/detail') }}"+"/"+id,
                                dataType: "json",
                                beforeSend: function() {
                                    var load = '<center style="font-size: 32px;"><i class="fa fa-spin fa-refresh"></i></center>';
                                    $('#bodyLeft').html(load);
                                    $('#bodyRight').html(load);
                                },
                                success: function (response) {
                                    // console.log(response.id);
                                    $('#bodyLeft').empty();
                                    $('#bodyRight').empty();
                                    if (response.gender == 'M') {
                                        var gender = 'Laki - laki';
                                    } else {
                                        var gender = 'Perempuan';
                                    }

                                    if (response.scholarship_status == 1) {
                                        var beasiswa = 'Ya';
                                    } else {
                                        var beasiswa = 'Tidak';
                                    }

                                    if (response.kjp_status == 1) {
                                        var kjp = 'Ya';
                                    } else {
                                        var kjp = 'Tidak';
                                    }

                                    if (response.alumnus_status == 1) {
                                        var alumni = 'Ya';
                                    } else {
                                        var alumni = 'Tidak';
                                    }
                                    var htmlLeft = '<tr>'+
                                            '<td>Nama Lengkap</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.full_name+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>NISN</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.nisn+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>NIK</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.nik+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>No KK</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.no_kk+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Alamat</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.address+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Agama</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.religion+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Tanggal Lahir</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.birth_date+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Tempat Lahir</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.birth_place+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Jenis Kelamin</td>'+
                                            '<td>:</td>'+
                                            '<td>'+gender+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Foto</td>'+
                                            '<td>:</td>'+
                                            '<td><a href="{{ asset("assets/images/profile") }}/'+response.image+'" title="open" target="_blank"><img style="width: 130px;" src="{{ asset("assets/images/profile") }}/'+response.image+'"></a></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Anak Ke</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.child_order+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Dari Total Saudara</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.total_sibling+'</td>'+
                                        '</tr>';

                                    var htmlRight = '<tr>'+
                                            '<td>No. Hp</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.phone_number+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Email</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.email+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Tempat Tinggal</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.residence_status+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Transportasi</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.transport+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Subjek Favorit</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.favorite_subject+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Minat Jurusan</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.major_interest+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Hobi</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.hobby+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Cita - cita</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.goals+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Status Beasiswa</td>'+
                                            '<td>:</td>'+
                                            '<td>'+beasiswa+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Pemberi Beasiswa</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.scholarship_foundation+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Status KJP</td>'+
                                            '<td>:</td>'+
                                            '<td>'+kjp+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Foto KJP</td>'+
                                            '<td>:</td>'+
                                            '<td><a href="{{ asset("assets/images/kjp") }}/'+response.kjp_image+'" title="open" target="_blank"><img style="width: 130px;" src="{{ asset("assets/images/kjp") }}/'+response.kjp_image+'"></a></td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Status Alumni</td>'+
                                            '<td>:</td>'+
                                            '<td>'+alumni+'</td>'+
                                        '</tr>'+
                                        '<tr>'+
                                            '<td>Status</td>'+
                                            '<td>:</td>'+
                                            '<td>'+response.status+'</td>'+
                                        '</tr>';
                                    
                                    $('#bodyLeft').html(htmlLeft);
                                    $('#bodyRight').html(htmlRight);
                                        
                                }
                            });
                        });

                        $('#datatable').DataTable({
                            responsive: true,
                            processing: true,
                            serverSide: true,
                            // scrollX: true,
                            ajax: {
                                url: '{{ url("/ppdb/profile/datatable") }}'
                            },
                            columns: [
                                {data: 'nisn'},
                                {data: 'full_name'},
                                {data: 'email'},
                                {data: 'address'},
                                {data: 'create_date'},
                                {data: 'modify_date'},
                                {data: 'action'},
                            ],
                            
                        });
                    });
                </script>  
@endsection