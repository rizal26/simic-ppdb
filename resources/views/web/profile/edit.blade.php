@extends('templates.main')
@section('content')

<style>
.container-img {
  position: relative;
  width: 25%;
}

.image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.container-img:hover .image {
  opacity: 0.3;
}

.container-img:hover .middle {
  opacity: 1;
}

/* ====================== */

.container-img-kjp {
  position: relative;
  width: 25%;
}

.image-kjp {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle-kjp {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.container-img-kjp:hover .image-kjp {
  opacity: 0.3;
}

.container-img-kjp:hover .middle-kjp {
  opacity: 1;
}

</style>

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">

                {{-- <div class="btn-group pull-right m-t-15">
                    <a href="" class="btn btn-default dropdown-toggle waves-effect">Add <span class="m-l-5"><i class="fa fa-plus"></i></span></a>
                </div> --}}

                <h4 class="page-title">Edit Profile</h4>
                <ol class="breadcrumb">
                    {{-- <li>
                        <a href="#">List</a>
                    </li> --}}
                </ol>
            </div>
        </div>

		<div class="row">
            <div class="col-sm-12">
                <div class="card-box table-responsive">
                    {{-- <h4 class="page-title"></h4> --}}
                    <p class="text-muted font-13 m-b-20"></p>
                    <br>
                    @if($errors->any())
                        @foreach($errors->all() as $error)
                            @if($errors->has('success'))
                                <div class="alert alert-success">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @else
                                <div class="alert alert-danger">
                                    <strong>{{ $error }}</strong>
                                </div>
                            @endif    
                        @endforeach
                    @endif
					<form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{ route('ppdb_profile.update', $data->id) }}">
						{{csrf_field()}}
						<div class="row">
							<div class="col-md-12">
								<div class="col-sm-6">
									<div class="form-group ">
										<label class="col-md-3 control-label">Email</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ $data->email }}" name="email" type="email" placeholder="Email">
										</div>
									</div>

									<div class="form-group" id="btn-pass">
                                        <label for="role" class="col-sm-3 control-label"></label>
                                        <div class="col-sm-9">
                                            <a href="javascript:void(0);" class="btn btn-info" id="pass">Change Password</a>
                                        </div>
                                    </div>
                                    <div class="form-group hidden" id="btn-newpass">
                                        <label for="password" class="col-sm-3 control-label">Password</label>
                                        <div class="col-sm-9">
                                            <input name="password" minlength="8" type="password" id="val-pass" class="form-control" id="password" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="form-group hidden" id="btn-renewpass">
                                        <label for="password_confirmation" class="col-sm-3 control-label">Re Password</label>
                                        <div class="col-sm-9">
                                            <input name="password_confirmation" type="password" id="val-repass" class="form-control" id="password_confirmation" placeholder="Retype Password">
                                        </div>
                                    </div>

									<div class="form-group ">
										<label class="col-md-3 control-label">NISN</label>
										<div class="col-md-9">
											<input class="form-control" type="text" value="{{ $data->nisn }}" name="nisn" placeholder="NISN">
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Nama Depan</label>
										<div class="col-md-9">
											<input class="form-control" type="text" value="{{ $data->first_name }}" name="first_name" placeholder="Nama Depan">
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Nama Belakang</label>
										<div class="col-md-9">
											<input class="form-control" type="text" value="{{ $data->last_name }}" name="last_name" placeholder="Nama Belakang">
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Nama Lengkap</label>
										<div class="col-md-9">
											<input class="form-control" type="text" value="{{ $data->full_name }}" name="full_name" placeholder="Nama Lengkap">
										</div>
									</div>
									

									<div class="form-group ">
										<label class="col-md-3 control-label">Agama</label>
										<div class="col-md-9">
											<input class="form-control" type="text" value="{{ $data->religion }}" name="religion" placeholder="Agama">
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Tempat Lahir</label>
										<div class="col-md-9">
											<input class="form-control" type="text" value="{{ $data->birth_place }}" name="birth_place" placeholder="Tempat Lahir">
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Tanggal Lahir</label>
										<div class="col-md-9">
											<div class="input-group">
												<input type="text" value="{{ date('m/d/Y', strtotime($data->birth_date)) }}" name="birth_date" class="form-control" placeholder="mm/dd/yyyy" id="datepicker">
												<span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
											</div>
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Jenis Kelamin</label>
										<div class="col-md-9">
											<select value="{{ old('gender') }}" name="gender" class="form-control">
												<option value="">- Select -</option>
												<option value="M" {{ $data->gender=='M'?'selected':'' }}>Pria</option>
												<option value="F" {{ $data->gender=='F'?'selected':'' }}>Wanita</option>
											</select>
										</div>
									</div>

									<div class="form-group">
                                        <label for="image" class="col-sm-3 control-label">Foto</label>
                                        <div class="col-sm-9">
                                            <input name="image" type="file" accept="image/*" class="form-control" id="foto" placeholder="Foto" disabled>
                                            <p  style="line-height: 2; font-size: 10px; margin-bottom: -10px;">*max 2 MB</p>
                                            <div class="container-img" style="padding-top: 10px">
                                                <img src="{{ asset('assets/images/profile/'.$data->image) }}"  class="image">
                                                <div class="middle">
                                                    <a href="javascript:void(0)" title="remove" id="close" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                    <a href="{{ asset('assets/images/profile/'.$data->image) }}" title="open" target="_blank" class="btn btn-xs btn-inverse"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

									<div class="form-group ">
										<label class="col-md-3 control-label">NIK</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ $data->nik }}" name="nik" type="text" placeholder="NIK" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Alamat</label>
										<div class="col-md-9">
											<textarea name="address" placeholder="Alamat" class="form-control" >{{ $data->address }}</textarea>
											{{-- <input class="form-control" value="" name="address" type="text" placeholder="Address" > --}}
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">No KK</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ $data->no_kk }}" name="no_kk" type="text" placeholder="Nomor Kartu Keluarga" >
										</div>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group ">
										<label class="col-md-3 control-label">Anak ke</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ $data->child_order }}" name="child_order" type="text" placeholder="Anak ke" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Dari Total Saudara</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ $data->total_sibling }}" name="total_sibling" type="text" placeholder="Dari Total Saudara" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Nomor Hp</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ $data->phone_number }}" name="phone_number" type="text" placeholder="Nomor Hp" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Tempat Tinggal</label>
										<div class="col-md-9">
											<select name="residence_status" id="" class="form-control">
												<option value="">- Select -</option>
												<option {{ $data->residence_status=='Menetap'?'selected':'' }}>Menetap</option>
												<option {{ $data->residence_status=='Kontrak'?'selected':'' }}>Kontrak</option>
											</select>
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Transportasi</label>
										<div class="col-md-9">
											<select name="transport" id="" class="form-control">
												<option value="">- Select -</option>
												<option {{ $data->transport=='Motor'?'selected':'' }}>Motor</option>
												<option {{ $data->transport=='Mobil'?'selected':'' }}>Mobil</option>
												<option {{ $data->transport=='Umum'?'selected':'' }}>Umum</option>
												<option {{ $data->transport=='Jalan Kaki'?'selected':'' }}>Jalan Kaki</option>
											</select>
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Subjek Favorit</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ $data->favorite_subject }}" name="favorite_subject" type="text" placeholder="Favorite Subject" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Minat Jurusan</label>
										<div class="col-md-9">
											<select name="major_interest" id="" class="form-control">
												<option value="">- Select -</option>
												<option {{ $data->major_interest=='IPA'?'selected':'' }}>IPA</option>
												<option {{ $data->major_interest=='IPS'?'selected':'' }}>IPS</option>
											</select>
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Hobi</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ $data->hobby }}" name="hobby" type="text" placeholder="Hobby" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Cita - cita</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ $data->goals }}" name="goals" type="text" placeholder="Goals" >
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Status Beasiswa</label>
										<div class="col-md-9">
											<select name="scholarship_status" id="" class="form-control">
												<option value="">- Select -</option>
												<option value="1" {{ $data->scholarship_status=='1'?'selected':'' }}>Ya</option>
												<option value="0" {{ $data->scholarship_status=='0'?'selected':'' }}>Tidak</option>
											</select>
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Pemberi Beasiswa</label>
										<div class="col-md-9">
											<select name="scholarship_foundation" id="" class="form-control">
												<option value="">- Select -</option>
												<option {{ $data->scholarship_foundation=='Perusahaan'?'selected':'' }}>Perusahaan</option>
												<option {{ $data->scholarship_foundation=='Pemerintah'?'selected':'' }}>Pemerintah</option>
											</select>
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Status KJP</label>
										<div class="col-md-9">
											<select name="kjp_status" id="" class="form-control">
												<option value="">- Select -</option>
												<option value="1" {{ $data->kjp_status=='1'?'selected':'' }}>Ya</option>
												<option value="0" {{ $data->kjp_status=='0'?'selected':'' }}>Tidak</option>
											</select>
										</div>
									</div>
									
									<div class="form-group">
                                        <label for="kjp_image" class="col-sm-3 control-label">Foto KJP</label>
                                        <div class="col-sm-9">
                                            <input name="kjp_image" type="file" accept="image/*" class="form-control" id="foto_kjp" placeholder="Foto KJP" disabled>
                                            <p  style="line-height: 2; font-size: 10px; margin-bottom: -10px;">*max 2 MB</p>
                                            <div class="container-img-kjp" style="padding-top: 10px">
                                                <img src="{{ asset('assets/images/kjp/'.$data->kjp_image) }}"  class="image-kjp">
                                                <div class="middle-kjp">
                                                    <a href="javascript:void(0)" title="remove" id="close_kjp" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                                                    <a href="{{ asset('assets/images/kjp/'.$data->kjp_image) }}" title="open" target="_blank" class="btn btn-xs btn-inverse"><i class="fa fa-eye"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Status Alumni</label>
										<div class="col-md-9">
											<select name="alumnus_status" id="" class="form-control">
												<option value="">- Select -</option>
												<option value="1" {{ $data->alumnus_status=='1'?'selected':'' }}>Ya</option>
												<option value="0" {{ $data->alumnus_status=='0'?'selected':'' }}>Tidak</option>
											</select>
										</div>
									</div>

									<div class="row">
										<div class="form-group ">
											<label class="col-md-3 control-label">Status</label>
											<div class="col-md-9">
												<input class="form-control" value="{{ $data->status }}" name="status" type="text" placeholder="Status" >
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group m-t-40">
							<div class="col-md-12">
								<div class="col-sm-3"></div>
                                <div class="col-sm-3">
                                    <button class="btn btn-inverse btn-block text-uppercase waves-effect waves-light" type="submit">
                                        Edit
                                    </button>
                                </div>
                                <div class="col-sm-3">
                                    <a href="{{ url('ppdb/profile') }}" class="btn btn-danger btn-block text-uppercase waves-effect waves-light">Batal</a>
                                </div>
                                <div class="col-sm-3"></div>
							</div>
						</div>

					</form>

				</div>
			</div>
        </div>
        
        <script>
            $(document).ready(function () {
                $("#pass").click(function() {
                    $("#btn-pass").addClass("hidden");
                    $("#btn-newpass").removeClass("hidden");
                    $("#btn-renewpass").removeClass("hidden");

                    $("#update").click(function() {
                        if ($("#val-pass").val() != $("#val-repass").val()) {
                            sweetAlert("Oops...","Password tidak sama","error");  
                            return false;
                        }
                    });
                });

                $( "#close" ).click(function() {
                    $( ".container-img" ).fadeOut( "slow" );
                    $("#foto").removeAttr("disabled");
                });

                $( "#close_kjp" ).click(function() {
                    $( ".container-img-kjp" ).fadeOut( "slow" );
                    $("#foto_kjp").removeAttr("disabled");
                });
            });
        </script>

@endsection