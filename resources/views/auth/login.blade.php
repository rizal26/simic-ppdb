@extends('templates.auth')
@section('content')   
    
    <div class="account-pages"></div>
        <div class="clearfix"></div>
        <div class="wrapper-page">
        	<div class=" card-box">
            <div class="panel-heading"> 
                <h3 class="text-center"> Simic <strong class="text-custom">PPDB</strong> </h3>
            </div> 

            @if($errors->any())
                @foreach($errors->all() as $error)
                    @if($errors->has('success'))
                        <div class="alert alert-success">
                            <strong>{{ $error }}</strong>
                        </div>
                    @else
                        <div class="alert alert-danger">
                            <strong>{{ $error }}</strong>
                        </div>
                    @endif    
                @endforeach
            @endif

            <div class="panel-body">
            <form class="form-horizontal m-t-20" method="POST" action="{{ url('/ppdb/login') }}">
                {{csrf_field()}}
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input class="form-control" type="text" value="{{ old('email') }}" name="email" required="" placeholder="Email">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input class="form-control" type="password" name="password" required="" placeholder="Password">
                    </div>
                </div>

                {{-- <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox-signup" type="checkbox">
                            <label for="checkbox-signup">
                                Remember me
                            </label>
                        </div>
                        
                    </div>
                </div> --}}
                
                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <button class="btn btn-inverse btn-block text-uppercase waves-effect waves-light" type="submit">Masuk</button>
                    </div>
                </div>

                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12">
                        <a href="javascript:void(0)" class="text-dark"><i class="fa fa-lock m-r-5"></i>Lupa password ?</a>
                    </div>
                </div>
            </form> 
            
            </div>   
            </div>                              
                <div class="row">
            	<div class="col-sm-12 text-center">
            		<p>Tidak punya akun ? <a href="{{ url('/ppdb/register') }}" class="text-primary m-l-5"><b>Daftar</b></a></p>
                        
                    </div>
            </div>
            
        </div>
@endsection