@extends('templates.auth')
@section('content')   

    <div class="account-pages"></div>
		<div class="clearfix"></div>
		<div class="wrapper-page" style="width: 1200px">
			<div class=" card-box">
				<div class="panel-heading">
					<h3 class="text-center"> Daftar <strong class="text-custom">PPDB</strong> </h3>
				</div>

				@if($errors->any())
					@foreach($errors->all() as $error)
						@if($errors->has('success'))
							<div class="alert alert-success">
								<strong>{{ $error }}</strong>
							</div>
						@else
							<div class="alert alert-danger">
								<strong>{{ $error }}</strong>
							</div>
						@endif    
					@endforeach
				@endif

				<div class="panel-body">
					<form class="form-horizontal" method="post" enctype="multipart/form-data" action="{{ url('/ppdb/register') }}">
						{{csrf_field()}}
						<div class="row">
							<div class="col-md-12">
								<div class="col-sm-6">
									<div class="form-group ">
										<label class="col-md-3 control-label">Email</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ old('email') }}" name="email" type="email" placeholder="Email">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-3 control-label">Password</label>
										<div class="col-md-9">
											<input class="form-control" type="password" name="password" id="password" minlength="8" placeholder="Password">
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-3 control-label">Ulangi Password</label>
										<div class="col-md-9">
											<input name="password_confirmation" type="password" class="form-control" id="password_confirmation" placeholder="Re-Password">
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">NISN</label>
										<div class="col-md-9">
											<input class="form-control" type="text" value="{{ old('nisn') }}" name="nisn" placeholder="NISN">
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Nama Depan</label>
										<div class="col-md-9">
											<input class="form-control" type="text" value="{{ old('first_name') }}" name="first_name" placeholder="Nama Depan">
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Nama Belakang</label>
										<div class="col-md-9">
											<input class="form-control" type="text" value="{{ old('last_name') }}" name="last_name" placeholder="Nama Belakang">
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Nama Lengkap</label>
										<div class="col-md-9">
											<input class="form-control" type="text" value="{{ old('full_name') }}" name="full_name" placeholder="Nama Lengkap">
										</div>
									</div>
									

									<div class="form-group ">
										<label class="col-md-3 control-label">Agama</label>
										<div class="col-md-9">
											<input class="form-control" type="text" value="{{ old('religion') }}" name="religion" placeholder="Agama">
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Tempat Lahir</label>
										<div class="col-md-9">
											<input class="form-control" type="text" value="{{ old('birth_place') }}" name="birth_place" placeholder="Tempat Lahir">
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Tanggal Lahir</label>
										<div class="col-md-9">
											<div class="input-group">
												<input type="text" value="{{ old('birth_date') }}" name="birth_date" class="form-control" placeholder="mm/dd/yyyy" id="datepicker">
												<span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
											</div>
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Jenis Kelamin</label>
										<div class="col-md-9">
											<select value="{{ old('gender') }}" name="gender" class="form-control">
												<option value="">- Select -</option>
												<option value="M" {{ old('gender')=='M'?'selected':'' }}>Pria</option>
												<option value="F" {{ old('gender')=='F'?'selected':'' }}>Wanita</option>
											</select>
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Foto (max 2mb)</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ old('image') }}" name="image" accept="image/*" type="file" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">NIK</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ old('nik') }}" name="nik" type="text" placeholder="NIK" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Alamat</label>
										<div class="col-md-9">
											<textarea name="address" placeholder="Alamat" class="form-control" >{{ old('address') }}</textarea>
											{{-- <input class="form-control" value="" name="address" type="text" placeholder="Address" > --}}
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">No KK</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ old('no_kk') }}" name="no_kk" type="text" placeholder="Nomor Kartu Keluarga" >
										</div>
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group ">
										<label class="col-md-3 control-label">Anak ke</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ old('child_order') }}" name="child_order" type="text" placeholder="Anak ke" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Dari Total Saudara</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ old('total_sibling') }}" name="total_sibling" type="text" placeholder="Dari Total Saudara" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Nomor Hp</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ old('phone_number') }}" name="phone_number" type="text" placeholder="Nomor Hp" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Tempat Tinggal</label>
										<div class="col-md-9">
											<select name="residence_status" id="" class="form-control">
												<option value="">- Select -</option>
												<option {{ old('residence_status')=='Menetap'?'selected':'' }}>Menetap</option>
												<option {{ old('residence_status')=='Kontrak'?'selected':'' }}>Kontrak</option>
											</select>
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Transportasi</label>
										<div class="col-md-9">
											<select name="transport" id="" class="form-control">
												<option value="">- Select -</option>
												<option {{ old('transport')=='Motor'?'selected':'' }}>Motor</option>
												<option {{ old('transport')=='Mobil'?'selected':'' }}>Mobil</option>
												<option {{ old('transport')=='Umum'?'selected':'' }}>Umum</option>
												<option {{ old('transport')=='Jalan Kaki'?'selected':'' }}>Jalan Kaki</option>
											</select>
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Subjek Favorit</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ old('favorite_subject') }}" name="favorite_subject" type="text" placeholder="Favorite Subject" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Minat Jurusan</label>
										<div class="col-md-9">
											<select name="major_interest" id="" class="form-control">
												<option value="">- Select -</option>
												<option {{ old('major_interest')=='IPA'?'selected':'' }}>IPA</option>
												<option {{ old('major_interest')=='IPS'?'selected':'' }}>IPS</option>
											</select>
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Hobi</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ old('hobby') }}" name="hobby" type="text" placeholder="Hobby" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Cita - cita</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ old('goals') }}" name="goals" type="text" placeholder="Goals" >
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Status Beasiswa</label>
										<div class="col-md-9">
											<select name="scholarship_status" id="" class="form-control">
												<option value="">- Select -</option>
												<option value="1" {{ old('scholarship_status')=='1'?'selected':'' }}>Ya</option>
												<option value="0" {{ old('scholarship_status')=='0'?'selected':'' }}>Tidak</option>
											</select>
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Pemberi Beasiswa</label>
										<div class="col-md-9">
											<select name="scholarship_foundation" id="" class="form-control">
												<option value="">- Select -</option>
												<option {{ old('scholarship_foundation')=='Perusahaan'?'selected':'' }}>Perusahaan</option>
												<option {{ old('scholarship_foundation')=='Pemerintah'?'selected':'' }}>Pemerintah</option>
											</select>
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Status KJP</label>
										<div class="col-md-9">
											<select name="kjp_status" id="" class="form-control">
												<option value="">- Select -</option>
												<option value="1" {{ old('kjp_status')=='1'?'selected':'' }}>Ya</option>
												<option value="0" {{ old('kjp_status')=='0'?'selected':'' }}>Tidak</option>
											</select>
										</div>
									</div>
									
									<div class="form-group ">
										<label class="col-md-3 control-label">Foto KJP (max 2mb)</label>
										<div class="col-md-9">
											<input class="form-control" value="{{ old('kjp_image') }}" name="kjp_image" accept="image/*" type="file" >
										</div>
									</div>

									<div class="form-group ">
										<label class="col-md-3 control-label">Status Alumni</label>
										<div class="col-md-9">
											<select name="alumnus_status" id="" class="form-control">
												<option value="">- Select -</option>
												<option value="1" {{ old('alumnus_status')=='1'?'selected':'' }}>Ya</option>
												<option value="0" {{ old('alumnus_status')=='0'?'selected':'' }}>Tidak</option>
											</select>
										</div>
									</div>

									<div class="row">
										<div class="form-group ">
											<label class="col-md-3 control-label">Status</label>
											<div class="col-md-9">
												<input class="form-control" value="{{ old('status') }}" name="status" type="text" placeholder="Status" >
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="form-group m-t-40">
							<div class="col-md-12">
								<div class="col-sm-4"></div>
									<div class="col-sm-4">
										<button class="btn btn-inverse btn-block text-uppercase waves-effect waves-light" type="submit">
											Daftar
										</button>
									</div>
								<div class="col-sm-4"></div>
							</div>
						</div>

					</form>

				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 text-center">
					<p>
						Sudah punya akun ?<a href="{{ url('/ppdb/login') }}" class="text-primary m-l-5"><b>Login</b></a>
					</p>
				</div>
			</div>

		</div>
@endsection