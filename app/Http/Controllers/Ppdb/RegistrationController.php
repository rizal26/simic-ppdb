<?php

namespace App\Http\Controllers\Ppdb;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;

use App\Models\FutureStudent;

class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('auth.register');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = Input::all();
        unset($data['_token']);

        $validator = Validator::make($data, [
            'email' => 'required|email|unique:future_students,email',
            'password' => 'required|confirmed|min:8',
            'nisn' => 'required|max:50',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'full_name' => 'required|max:100',
            'religion' => 'required|max:15',
            'birth_place' => 'required|max:50',
            'birth_date' => 'required|date_format:m/d/Y',
            'gender' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'nik' => 'required|max:20',
            'address' => 'required|min:5',
            'no_kk' => 'required|max:20',
            'child_order' => 'required|numeric',
            'total_sibling' => 'required|numeric',
            'phone_number' => 'required|numeric|digits_between:11,13',
            'residence_status' => 'required',
            'transport' => 'required',
            'favorite_subject' => 'required|max:50',
            'major_interest' => 'required',
            'hobby' => 'required',
            'goals' => 'required',
            'scholarship_status' => 'required',
            // 'scholarship_foundation' => 'required',
            'kjp_status' => 'required',
            'kjp_image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'alumnus_status' => 'required',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return Redirect::to('/ppdb/register')->withErrors($errors)->withInput();
        } 
        
        unset($data['password_confirmation']);
        $data['password'] = Hash::make($data['password']);
        
        $date = strtotime($data['birth_date']);
        $data['birth_date'] = date("Y-m-d", $date);
        // dd($data);
        
        $foto = $data['image'];
        if ($foto) {
            $destinationPath = public_path('assets/images/profile/'); // upload path
            $profileImage = date('dmyhis') . "-". $data['full_name'] . '.' . $foto->getClientOriginalExtension();
            $profileImageFix = str_replace(')', '', str_replace('(', '', str_replace(' ', '_', $profileImage)));
            $foto->move($destinationPath, $profileImageFix);
            $data['image'] = "$profileImageFix";
        } else {
            $errors = array('error' => 'Foto Error, Harap hubungi administrator');
            return Redirect::to('/ppdb/register')->withErrors($errors)->withInput();
        }

        $foto_kjp = $data['kjp_image'];
        if ($foto_kjp) {
            $destinationPath_kjp = public_path('assets/images/kjp/'); // upload path
            $profileImage_kjp = date('dmyhis') . "-" . $data['full_name'] . '.' . $foto_kjp->getClientOriginalExtension();
            $profileImageFix_kjp = str_replace(')', '', str_replace('(', '', str_replace(' ', '_', $profileImage_kjp)));
            $foto_kjp->move($destinationPath_kjp, $profileImageFix_kjp);
            $data['kjp_image'] = "$profileImageFix_kjp";
        } else {
            $errors = array('error' => 'Foto Error, Harap hubungi administrator');
            return Redirect::to('/ppdb/register')->withErrors($errors)->withInput();
        }

        $saveState = (new FutureStudent)->store($data);
        if (!$saveState) {
            $errors = array('error' => 'API Error, Harap hubungi administrator');
            return Redirect::to('/ppdb/register')->withErrors($errors);
        } 
        
        $success = array('success' => 'Terimakasih pendaftaran akun berhasil.');
        return Redirect::to('ppdb/login')->withErrors($success);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
