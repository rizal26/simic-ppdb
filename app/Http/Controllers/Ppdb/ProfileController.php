<?php

namespace App\Http\Controllers\Ppdb;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\File;

use App\Models\FutureStudent;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('web.profile.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('ppdb/login')->withErrors(['error' => 'Silahkan login kembali.']);
        }
        
        if ($id != Session::get('id')) {
            return redirect()->back()->withErrors(['Maaf terjadi kesalahan'])->withInput();
        }
        
        $data = (new FutureStudent())->getBy(['id' => $id])[0];
        return view('web.profile.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('ppdb/login')->withErrors(['error' => 'Silahkan login kembali.']);
        }
        
        $id = $request->id;
        $data = Input::all();
        $data['id'] = $id;
        unset($data['_token']);
        // dd($data);

        $input = array(
            'email' => 'required|email',
            // 'password' => 'confirmed|min:8',
            'nisn' => 'required|max:50',
            'first_name' => 'required|max:50',
            'last_name' => 'required|max:50',
            'full_name' => 'required|max:100',
            'religion' => 'required|max:15',
            'birth_place' => 'required|max:50',
            'birth_date' => 'required|date_format:m/d/Y',
            'gender' => 'required',
            // 'image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'nik' => 'required|max:20',
            'address' => 'required|min:5',
            'no_kk' => 'required|max:20',
            'child_order' => 'required|numeric',
            'total_sibling' => 'required|numeric',
            'phone_number' => 'required|numeric|digits_between:11,13',
            'residence_status' => 'required',
            'transport' => 'required',
            'favorite_subject' => 'required|max:50',
            'major_interest' => 'required',
            'hobby' => 'required',
            'goals' => 'required',
            'scholarship_status' => 'required',
            // 'scholarship_foundation' => 'required',
            'kjp_status' => 'required',
            // 'kjp_image' => 'image|mimes:jpeg,png,jpg|max:2048',
            'alumnus_status' => 'required',
            'status' => 'required',
        );

        if ($data['password'] != null) {
            $input['password'] = 'confirmed|min:8';
        }

        $foto = $request->file('image');
        if (isset($foto)) {
            $input['image'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        $foto_kjp = $request->file('kjp_image');
        if (isset($foto_kjp)) {
            $input['kjp_image'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        $validator = Validator::make($data, $input);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->back()->withErrors($errors)->withInput();
        } 
        
        unset($data['password_confirmation']);
        $password = $request->input('password');
        if ($password != null) {
            $data['password'] = Hash::make($data['password']);
        } else {
            unset($data['password']);
        }
        
        $date = strtotime($data['birth_date']);
        $data['birth_date'] = date("Y-m-d", $date);
        // dd($data);
        $profile = (new FutureStudent())->getBy(['id' => $id])[0];
        $foto = $request->file('image');
        if (isset($foto)) {
            if ($foto) {
                $oldPath = public_path('assets/images/profile/' . $profile->image);
                unlink($oldPath);
                $destinationPath = public_path('assets/images/profile/'); // upload path
                $profileImage = date('dmyhis') . "-". $data['full_name'] . '.' . $foto->getClientOriginalExtension();
                $profileImageFix = str_replace(')', '', str_replace('(', '', str_replace(' ', '_', $profileImage)));
                $foto->move($destinationPath, $profileImageFix);
                $data['image'] = "$profileImageFix";
            } else {
                $errors = array('error' => 'Foto Error, Harap hubungi administrator');
                return redirect()->back()->withErrors($errors);
            }
        }

        $foto_kjp = $request->file('kjp_image');
        if (isset($foto_kjp)) {
            if ($foto_kjp) {
                $oldPath_kjp = public_path('assets/images/kjp/' . $profile->kjp_image);
                unlink($oldPath_kjp);
                $destinationPath_kjp = public_path('assets/images/kjp/'); // upload path
                $profileImage_kjp = date('dmyhis') . "-" . $data['full_name'] . '.' . $foto_kjp->getClientOriginalExtension();
                $profileImageFix_kjp = str_replace(')', '', str_replace('(', '', str_replace(' ', '_', $profileImage_kjp)));
                $foto_kjp->move($destinationPath_kjp, $profileImageFix_kjp);
                $data['kjp_image'] = "$profileImageFix_kjp";
            } else {
                $errors = array('error' => 'Foto Error, Harap hubungi administrator');
                return redirect()->back()->withErrors($errors);
            }
        }

        $saveState = (new FutureStudent)->edit($data);
        if (!$saveState) {
            $errors = array('error' => 'API Error, Harap hubungi administrator');
            return redirect()->back()->withErrors($errors);
        } 
        
        $success = array('success' => 'Data berhasil diedit');
        return Redirect::to('ppdb/profile')->withErrors($success);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function detail($id)
    {
        if(!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('ppdb/login')->withErrors(['error' => 'Silahkan login kembali.']);
        }

        $data = (new FutureStudent())->getBy(['id' => $id]);
        return json_encode($data[0]);
    }
    
    public function datatable() 
    {
        if(!(Session::has('login') && Session::get('login'))) {
            return Redirect::to('ppdb/login')->withErrors(['error' => 'Silahkan login kembali.']);
        }

        $user = FutureStudent::where(['id' => Session::get('id')]);
        $data = Datatables::of($user)
            ->addColumn('action', function ($user) {
                return '
                    <a href="#" class="btn btn-xs btn-inverse" title="View" data-id="'.$user->id.'" data-act="deactivate" data-toggle="modal" data-target="#detail"><i class="fa fa-eye"></i></a>
                    <a href="'.url('ppdb/profile/edit/'.$user->id).'" class="btn btn-xs btn-default" title="Edit"><i class="fa fa-pencil"></i></a>
                ';
            })
            ->addColumn('create_date', function ($user) {
                return date('d-M-Y H:i', strtotime($user->created_at));
            })
            ->addColumn('modify_date', function ($user) {
                if($user->modified_at) {
                    $modify_date = date('d-M-Y H:i', strtotime($user->modified_at));
                } else {
                    $modify_date = ' - ';
                }
                return $modify_date;
            })
            ->make(true);
        // dd($data);
        return $data;
        
    }
}
