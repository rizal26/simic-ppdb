<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class FutureStudent extends Model
{
    public function store($data)
    {
        $results = DB::table('future_students')->insert($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }

    public function getBy($data) 
    {
        $results = DB::table('future_students')->where($data)->get();
       	if ($results) {
	        // Session::put('userdata', $results);
	        return $results;
       	} else {
       		return false;
       	}
    }

    public function edit($data)
    {
        $results = DB::table('future_students')
            ->where('id', $data['id'])
            ->update($data);
        if ($results) {
            return true;
        } else {
            return false;
        }
    }
}
